from typing import ClassVar
import pyautogui
import pygame as pg


class GuiSettings:
    ONE_TIME_COUNT: ClassVar[int] = 0  # helps with one-time actions

    def __init__(self):
        """Initialize the game's settings."""
        # Screen settings

        # calculate game size as a percentage of device screen size
        device_width, device_height = pyautogui.size()
        self.screenPct: float = float(0.75)

        # ratio to original hard-coded values of 1280 (w) x 720 (h) in the original code
        ratio_height: float = device_height / 720.0
        ratio_width: float = device_width / 1280.0
        if GuiSettings.ONE_TIME_COUNT == 0:
            print(f'ratios: height: {ratio_height:.2f}, width: {ratio_width:.2f}')

        # round scaled width and height to multiple of 100
        game_width: int = int((device_width * self.screenPct // 100) * 100)
        game_height: int = int((device_height * self.screenPct // 100) * 100)
        if GuiSettings.ONE_TIME_COUNT == 0:
            print(f'game width: {game_width}, height: {game_height}')

        self.screen_width = game_width
        self.screen_height = game_height

        self.SCREEN_RECTANGLE = pg.Rect(0, 0, self.screen_width, self.screen_height)

        # radius of the circle that can be moved
        radius = 40 # original hard-coded value
        self.circle_radius = radius * ratio_height
        if GuiSettings.ONE_TIME_COUNT == 0:
            print(f'circle radius: {self.circle_radius}')
        self.circle_color = 'yellow'
        self.circle_move_delta = int(300 * ratio_height) # 300 was original hard-coded value
        if GuiSettings.ONE_TIME_COUNT == 0:
            print(f'circle move delta: {self.circle_move_delta}')

        GuiSettings.ONE_TIME_COUNT += 1

