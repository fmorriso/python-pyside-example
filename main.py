import sys

import PySide6.QtCore
from PySide6.QtWidgets import QApplication
import pyautogui as pg

from input_utilities import InputUtils
from output_utilities import OutputUtils


def get_python_version() -> str:
    return f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"


def main() -> None:
    OutputUtils.display_message("when in the course of human events", "quote")

    val: int = InputUtils.get_whole_number("Generic Whole Number", "Enter a whole number", )
    print(f'You entered {val}')

    n: int = InputUtils.get_whole_number_within_range("Age", "Enter your age", min=1, max=120)
    print(f'You entered {n}')


if __name__ == '__main__':
    msg = f'Python version: {get_python_version()}'
    print(msg)
    print(f'PySide6 version: {PySide6.__version__}')
    print(f'Qt version: {PySide6.QtCore.__version__}')
    print(f'PyAutoGUI version: {pg.__version__}')
    main()
