import sys

from typing import ClassVar

from PySide6.QtWidgets import QDialog, QMessageBox
from PySide6 import QtWidgets, QtGui
from PySide6.QtWidgets import QApplication, QMainWindow, QMessageBox


class OutputUtils(QDialog):

    parent_app: ClassVar[QtGui.QGuiApplication] = None
    parent_window: ClassVar[QMainWindow] = None

    @staticmethod
    def initialize_parent_app():
        if not QtWidgets.QApplication.instance():
            OutputUtils.parent_app = QApplication(sys.argv)
        else:
            OutputUtils.parent_app = QApplication.instance()

    @staticmethod
    def display_message(msg: str, title: str, parent=None) -> None:
        #app = QApplication(sys.argv)
        OutputUtils.initialize_parent_app()
        buttons = QMessageBox.StandardButton.Ok
        QMessageBox.information(parent, title, msg, buttons)

