from PySide6.QtWidgets import QMainWindow

from gui_settings import GuiSettings


class DummyMainWindow(QMainWindow):
    def __init__(self, title=''):
        super(DummyMainWindow, self).__init__()
        self.settings = GuiSettings(0.45, 100)

        self.settings = GuiSettings(0.45, 100)
        self.setMinimumSize(self.settings.scaled_width, self.settings.scaled_height)
        self.setWindowTitle(title)
        self.title = title
        # self.show()
